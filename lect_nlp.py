# -*- coding: utf-8 -*-
"""
Created on Wed Sep 25 09:45:46 2019

@author: User
"""
# will be used later to process NL data
import gensim
import numpy
import pymorphy3
# needed for service porpuse (dose not matter for business logic understanding)
from nltk.data import find

'''
 will be used to process NL sentences producing list of tuples like [('Украинский', 'ADJ'), ('бок.... ]
 must be trained before use
'''
from nltk.tag.perceptron import PerceptronTagger

'''
named constant which defines directory to store variables binary dumps
dumping variable values could help to avoid reapplying greedy computations on each launch 
'''
BIN_DUMP_DIR = 'bin_dumps/'
LOAD_DUMP = 1
# would be used to make aforementioned binary dumps
import pickle
# would be used to represent text data from "lenta.ru" corpus
import pandas as pd

# actually would not be used
RUS_PICKLE = 'taggers/averaged_perceptron_tagger_ru/averaged_perceptron_tagger_ru.pickle'
# named constant which defines path to train set witch would be used to setup Part Of Speech(POS) tagger (line:15)
PATH2CORP = dict(zip(['dev', 'train', 'pos'], ['RUPOS/ru_pos_dev', 'RUPOS/ru_pos_train', 'RUPOS/ru_pos_test']))

global pretrained_vector_model, news_vec_dictionary, text_corp, news_back_index, news_vecs, PMA, titles

'''
 create and save a dump of given variable
 l - is value to save in filesystem
 fn - is an alias we would use to reload "l" value
 d - is directory to store dump file (in particular case BIN_DUMP_DIR defined at line 21) 
'''


def store_bin(l, fn, d=BIN_DUMP_DIR): pickle.dump(l, open(d + fn + ".bin", "wb"))


'''
 obtain object from previously stored dump
 fn - is an alias we have used to store object (while call store_bin(%object%,%name%))
 d - is directory where dump files stored (in particular case BIN_DUMP_DIR defined at line 21) 
'''


def restore_bin(fn, d=BIN_DUMP_DIR): return pickle.load(open(d + fn + '.bin', "rb"))


def _get_tagger(lang=None, ts=None):
    '''
        will not be used, defined to provide backward compatibility with older projects
    '''
    if lang == 'rus':
        tagger = PerceptronTagger(False)
        if (ts != None):
            tagger.train(ts)
        else:
            ap_russian_model_loc = 'file:' + str(find(RUS_PICKLE))
            tagger.load(ap_russian_model_loc)
    else:
        tagger = PerceptronTagger()
    return tagger


def train_active(ts=None):
    '''
    When called with specifically structured Training Set (a.k.a ts) witch
    contains NL sentences slitted to single words where each word has given
    (provided by expert or other valid source) POS tag returns Tagger object.
    Trained tagger could forecast POS for given word list (in assumption it is sentence)

    After first call, when training process would be finished, we should make
    dump of returned object with 'store_bin' so in future launches we may skip train procedure
    and simply restore binary dump of trained structure
    '''
    tagger = PerceptronTagger(False)
    if (ts != None):
        print("Start training with " + str(len(ts)) + " of sentences")
        tagger.train(ts)
        print("Training succesfully finished")
    else:
        print("No training set was passed, trying to use default!")
        ap_russian_model_loc = 'file:' + str(find(RUS_PICKLE))
        tagger.load(ap_russian_model_loc)
    set_active(tagger)
    return tagger


def set_active(t):
    '''
        Needed as abstraction layer to be sure we
                                    - got Tagger object
                                    - it is singleton and visible over this script
                                    - it has predefined name 'A_TAGGER'
    '''
    global A_TAGGER
    A_TAGGER = t
    return A_TAGGER


'''
Calling train_active with list of example sentences restored from dump
calculation could take big amount of time
'''


def get_word_normal_form(w): return PMA.parse(w)[0].normal_form


def get_word_vector_or_die(e):
    i = pretrained_vector_model.key_to_index.get(e)
    if isinstance(i, int):
        return pretrained_vector_model.vectors[i]
    else:
        return ""


def get_news_vectors(num):
    a = list(filter(lambda x: x[1] in ["ADJ", "NOUN", "VERB"], A_TAGGER.tag(list(gensim.utils.simple_tokenize(text_corp[num])))))
    return list(filter(lambda x: isinstance(x, numpy.ndarray), list(
        map(get_word_vector_or_die, list(map(lambda e: get_word_normal_form(e[0]) + "_" + e[1], list(a)))))))


def get_joined_vector(num):
    zv = numpy.zeros([300], numpy.float32)
    try:
        zv = numpy.asarray(numpy.mat(get_news_vectors(num)).mean(0).tolist()[0], numpy.float32)
    except:
        zv.fill(-1)
        print(text_corp[num])
    finally:
        return dict(zip([i for i in range(0, 300)], zv))


def get_news_vec_dictionary():
    global news_vecs# = dict(zip([i for i in range(0, len(text_corp))], list(map(get_joined_vector, text_corp))))
    news_vecs = dict(zip([i for i in range(0, len(text_corp))], list(map(get_joined_vector, text_corp))))
    return news_vecs


def describe_news_item(num):
    print(text_corp[num])
    s = pretrained_vector_model.similar_by_vector(get_joined_vector(num), 10)
    print(s)
    # print(get_joined_vector(5))


def get_similar_news_by_id(num, topn=10, print_full=False):
    print(text_corp[num])
    print("*************************")
    pw = dict(zip([i for i in range(0, len(text_corp))],
                  [gensim.matutils.cossim(news_vecs.get(num), others) for others in news_vecs.values()]))
    pw = sorted(pw.items(), key=lambda x: x[1])
    pw.reverse()
    idx = [pw[x][0] for x in range(0, topn)]

    list(map(lambda z:print(z), [titles[x]+(text_corp[x] if print_full else "") for x in idx]))
    return idx


def tsne_plot(n=500,perplexity=25,n_iter=6500):
    "Creates and TSNE model and plots it"
    labels = []
    tokens=[]
    colormap=['#dc143c','#ffb6c1','#ffaeb9','#eea2ad','#cd8c95','#8b5f65','#ffc0cb','#ffb5c5','#eea9b8','#cd919e','#8b636c','#db7093','#ff82ab','#ee799f','#cd6889','#8b475d','#fff0f5','#eee0e5','#cdc1c5','#8b8386','#ff3e96','#ee3a8c','#cd3278','#8b2252','#ff69b4','#ff6eb4','#ee6aa7','#cd6090','#8b3a62','#872657','#ff1493','#ee1289','#cd1076','#8b0a50','#ff34b3','#ee30a7','#cd2990','#8b1c62','#c71585','#d02090','#da70d6','#ff83fa','#ee7ae9','#cd69c9','#8b4789','#d8bfd8','#ffe1ff','#eed2ee','#cdb5cd','#8b7b8b','#ffbbff','#eeaeee','#cd96cd','#8b668b','#dda0dd','#ee82ee','#ff00ff','#ee00ee','#cd00cd','#8b008b','#800080','#ba55d3','#e066ff','#d15fee','#b452cd','#7a378b','#9400d3','#9932cc','#bf3eff','#b23aee','#9a32cd','#68228b','#4b0082','#8a2be2','#9b30ff','#912cee','#7d26cd','#551a8b','#9370db','#ab82ff','#9f79ee','#8968cd','#5d478b','#483d8b','#8470ff','#7b68ee','#6a5acd','#836fff','#7a67ee','#6959cd','#473c8b','#f8f8ff','#e6e6fa','#0000ff','#0000ee','#0000cd','#00008b','#000080','#191970','#3d59ab','#4169e1','#4876ff','#436eee','#3a5fcd','#27408b','#6495ed','#b0c4de','#cae1ff','#bcd2ee','#a2b5cd','#6e7b8b','#778899','#708090','#c6e2ff','#b9d3ee','#9fb6cd','#6c7b8b','#1e90ff','#1c86ee','#1874cd','#104e8b','#f0f8ff','#4682b4','#63b8ff','#5cacee','#4f94cd','#36648b','#87cefa','#b0e2ff','#a4d3ee','#8db6cd','#607b8b','#87ceff','#7ec0ee','#6ca6cd','#4a708b','#87ceeb','#00bfff','#00b2ee','#009acd','#00688b','#33a1c9','#add8e6','#bfefff','#b2dfee','#9ac0cd','#68838b','#b0e0e6','#98f5ff','#8ee5ee','#7ac5cd','#53868b','#00f5ff','#00e5ee','#00c5cd','#00868b','#5f9ea0','#00ced1','#f0ffff','#e0eeee','#c1cdcd','#838b8b','#e0ffff','#d1eeee','#b4cdcd','#7a8b8b','#bbffff','#aeeeee','#96cdcd','#668b8b','#2f4f4f','#97ffff','#8deeee','#79cdcd','#528b8b','#00ffff','#00eeee','#00cdcd','#008b8b','#008080','#48d1cc','#20b2aa','#03a89e','#40e0d0','#808a87','#00c78c','#7fffd4','#76eec6','#66cdaa','#458b74','#00fa9a','#f5fffa','#00ff7f','#00ee76','#00cd66','#008b45','#3cb371','#54ff9f','#4eee94','#43cd80','#2e8b57','#00c957','#bdfcc9','#3d9140','#f0fff0','#e0eee0','#c1cdc1','#838b83','#8fbc8f','#c1ffc1','#b4eeb4','#9bcd9b','#698b69','#98fb98','#9aff9a','#90ee90','#7ccd7c','#548b54','#32cd32','#228b22','#00ff00','#00ee00','#00cd00','#008b00','#008000','#006400','#308014','#7cfc00','#7fff00','#76ee00','#66cd00','#458b00','#adff2f','#caff70','#bcee68','#a2cd5a','#6e8b3d','#556b2f','#6b8e23','#c0ff3e','#b3ee3a','#9acd32','#698b22','#fffff0','#eeeee0','#cdcdc1','#8b8b83','#f5f5dc','#ffffe0','#eeeed1','#cdcdb4','#8b8b7a','#fafad2','#ffff00','#eeee00','#cdcd00','#8b8b00','#808069','#808000','#bdb76b','#fff68f','#eee685','#cdc673','#8b864e','#f0e68c','#eee8aa','#fffacd','#eee9bf','#cdc9a5','#8b8970','#ffec8b','#eedc82','#cdbe70','#8b814c','#e3cf57','#ffd700','#eec900','#cdad00','#8b7500','#fff8dc','#eee8cd','#cdc8b1','#8b8878','#daa520','#ffc125','#eeb422','#cd9b1d','#8b6914','#b8860b','#ffb90f','#eead0e','#cd950c','#8b6508','#ffa500','#ee9a00','#cd8500','#8b5a00','#fffaf0','#fdf5e6','#f5deb3','#ffe7ba','#eed8ae','#cdba96','#8b7e66','#ffe4b5','#ffefd5','#ffebcd','#ffdead','#eecfa1','#cdb38b','#8b795e','#fce6c9','#d2b48c','#9c661f','#ff9912','#faebd7','#ffefdb','#eedfcc','#cdc0b0','#8b8378','#deb887','#ffd39b','#eec591','#cdaa7d','#8b7355','#ffe4c4','#eed5b7','#cdb79e','#8b7d6b','#e3a869','#ed9121','#ff8c00','#ff7f00','#ee7600','#cd6600','#8b4500','#ff8000','#ffa54f','#ee9a49','#cd853f','#8b5a2b','#faf0e6','#ffdab9','#eecbad','#cdaf95','#8b7765','#fff5ee','#eee5de','#cdc5bf','#8b8682','#f4a460','#c76114','#d2691e','#ff7f24','#ee7621','#cd661d','#8b4513','#292421','#ff7d40','#ff6103','#8a360f','#a0522d','#ff8247','#ee7942','#cd6839','#8b4726','#ffa07a','#ee9572','#cd8162','#8b5742','#ff7f50','#ff4500','#ee4000','#cd3700','#8b2500','#5e2612','#e9967a','#ff8c69','#ee8262','#cd7054','#8b4c39','#ff7256','#ee6a50','#cd5b45','#8b3e2f','#8a3324','#ff6347','#ee5c42','#cd4f39','#8b3626','#fa8072','#ffe4e1','#eed5d2','#cdb7b5','#8b7d7b','#fffafa','#eee9e9','#cdc9c9','#8b8989','#bc8f8f','#ffc1c1','#eeb4b4','#cd9b9b','#8b6969','#f08080','#cd5c5c','#ff6a6a','#ee6363','#8b3a3a','#cd5555','#a52a2a','#ff4040','#ee3b3b','#cd3333','#8b2323','#b22222','#ff3030','#ee2c2c','#cd2626','#8b1a1a','#ff0000','#ee0000','#cd0000','#8b0000','#800000','#8e388e','#7171c6','#7d9ec0','#388e8e','#71c671','#8e8e38','#c5c1aa','#c67171','#555555','#1e1e1e','#282828','#515151','#5b5b5b','#848484','#8e8e8e','#aaaaaa','#b7b7b7','#c1c1c1','#eaeaea','#f4f4f4','#ffffff','#f5f5f5','#dcdcdc','#d3d3d3','#c0c0c0','#a9a9a9','#808080','#696969','#000000']
    numpy.random.shuffle(colormap)
    colors=[]

    idxss=list(set(numpy.random.randint(0,len(news_vecs),n)))
    
    tokens=numpy.matrix([numpy.asarray(list(news_vecs[i].values()),dtype=numpy.float32) for i in idxss],dtype=numpy.float32)
    tokens=numpy.asarray(tokens);
    

    for i in idxss:
        labels.append(news.iloc[i].tags)
        
    lset=list(set(labels));   
    for i in idxss:
        try:
            colors.append(colormap[lset.index(news.iloc[i].tags)])
        except:
            colors.append('#000000')
        
        
    tsne_model = TSNE(perplexity=25, n_components=2, init='pca', n_iter=6500, random_state=23)
    new_values = tsne_model.fit_transform(tokens)

    x = []
    y = []
    for value in new_values:
        x.append(value[0])
        y.append(value[1])
        
    plt.figure(figsize=(25, 25)) 
    for i in range(len(x)):
        plt.scatter(x[i],y[i],c=colors[i])
        plt.annotate(labels[i],
                     xy=(x[i], y[i]),
                     xytext=(5, 2),
                     textcoords='offset points',
                     ha='right',
                     va='bottom',
                     fontsize=3)
    plt.show()



def gsn(n,topn=10,pf=False):
    return get_similar_news_by_id(n,topn,pf)


PMA = pymorphy3.MorphAnalyzer()

if LOAD_DUMP:
    set_active(restore_bin('taggie'))
    news = restore_bin("news")
    pretrained_vector_model = restore_bin("ruwiki_model")
    news_back_index = restore_bin("bi")
    news_vecs = restore_bin("nv")
    news_vec_dictionary = news.to_dict()
    text_corp = news_vec_dictionary.get('text')
    titles = news_vec_dictionary.get('title')

else:
    '''
     for future calls
        line 102: train_active(restore_bin(PATH2CORP['train']))
     should be replaced with
        set_active(restore_bin('taggie'))
    '''
    train_active(restore_bin(PATH2CORP['train']))
    # ASAP save training result to binary dump
    store_bin(A_TAGGER, 'taggie')
    '''
    let read data from large set of news
    then store it with store_bin routine
    '''
    news = pd.read_csv('.\\data\\news_lenta.csv', low_memory=False, iterator=True, chunksize=100000).read(4000)
    store_bin(news, 'news')

    news_vec_dictionary = news.to_dict()
    text_corp = news_vec_dictionary.get('text')
    titles = news_vec_dictionary.get('title')

    w2vDict = ".\\w2v\\ruwikiruscorpora_upos_skipgram_300_2_2018.vec.gz"
    pretrained_vector_model = gensim.models.KeyedVectors.load_word2vec_format(w2vDict)
    store_bin(pretrained_vector_model, "ruwiki_model")

    news_back_index = list(map(get_joined_vector, [i for i in range(1, news.size)]))
    store_bin(news_back_index, "bi")


    get_news_vec_dictionary()
    store_bin(news_vecs, "nv")






gsn(74,20,False)
