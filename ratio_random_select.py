import pandas as pd
import random
import numpy as np

EXLS_FILE='d://_WORK//_PAWLIN//inform_model_TXT.xlsx'
PARSED_FORM=None
PTR=98
DTYPE_MAP={
    'DUPLICATE':np.bool,
    'ANALYTICS':np.bool	,
    'AGENTS':np.bool,
    'URGENT':np.bool,
    'CLASSIFIED':np.bool}

DATA_CONV={
    'SOURCE_ID':(lambda x:x if isinstance(x, (int, float, complex)) else -1),
    'CRITERIAS':(lambda x:",".join(list(map(lambda y:'['+y.strip()+']',str(x).split(','))))),
    'TITLE':(lambda x:" ".join(str(x).split())),
    'NEWS_TEXT':(lambda x:" ".join(str(x).split())),}

def load_xls():
    global PARSED_FORM
    PARSED_FORM= pd.ExcelFile(EXLS_FILE).parse(sheet_name='Выборка 10 - финал', usecols=18, dtype=DTYPE_MAP, converters=DATA_CONV)


def getNumbers(min,max,count):
    random_idxs=[]
    all_idx=[i for i in range(min,max)]
    while(len(random_idxs)<count):
        nr=random.randint(0,len(all_idx)-1)
        n=all_idx[nr]
        random_idxs.append(n)
        all_idx.remove(n)
    return random_idxs


def getRatio(percent,df):
    count=np.math.floor(len(df) / 100 * percent)
    rnd_to_remove=getNumbers(0,len(df),count)
    choosen_list=list(map(lambda x:df.loc[x],rnd_to_remove))
    return list([rnd_to_remove,choosen_list])

load_xls();
print("Source length after file load: "+str(len(PARSED_FORM)))
PERCENT30AND_NUMS2REMOVE=getRatio(PTR,PARSED_FORM)

PARSED_FORM = PARSED_FORM.drop(index=PERCENT30AND_NUMS2REMOVE[0]);
PARSED_FORM.index = range(len(PARSED_FORM));

PERCENT30=PERCENT30AND_NUMS2REMOVE[1]

print('Source length (after 30% exclusion): '+str(len(PARSED_FORM)))
print('Randomly selected 30% of data: '+str(len(PERCENT30)))
